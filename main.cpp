#include <emscripten.h>
#include <stdio.h>
#include <malloc.h>

void one_iter();

int main() {
    // 1024 byte buffer for sharing between C++ and JavaScript
    char* ptr1 = (char*)malloc(1024);

    // Setting byte with index 1 to value 16.
    // Later to be read from JavaScript
    *(ptr1+1) = (char)16;

    // Executing inline javascript and calling function from 
    // other file.
    // A pointer to buffer is passed to the function.
    // Function reads from buffer and prints result to console.
    // See: library.js::readFromBuffer
    EM_ASM_({
        readFromBuffer( $0);
    }, (int)ptr1);

    //EM_ASM(myTestJSFunction()); <- woudl be call without parameter passing
    EM_ASM_(myTestJSFunction($0, $1), 100, 200);

    // Executing some arbitrary javascript, passing integers in.
    int a = EM_ASM_INT({
        return $0 * $1;
    }, 100, 2);

    printf("received from js: %d\n", a);

    // void emscripten_set_main_loop(em_callback_func func, int fps, int simulate_infinite_loop);
    emscripten_set_main_loop(one_iter, 0, 1);
}

// Function to be called from JavaScript
extern "C" int addTwoVars(int a, int b)
{
    return a + b;
}

// The "main loop" function.
void one_iter() {
    static int a = 1;

    if (a < 4)
    {
        printf("requestAnimationFrame loop iteration: %d\n", a);
    }

    a++;
}