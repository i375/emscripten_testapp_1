# emscripten_testapp_1

This is a sample of using emscripten for compiling C++ code and
making JavaScript and C++ interact with each other.

This also a VisualStudio project and can be viewed and edited with VisualStudio.

# Building

For building first run ```emsdk_env.bat``` which in my case is located at
```c:\emsdk_portable_64bit\emsdk_env.bat``` and then run ```build.bat``` this will produce
```index.html``` and ```index.js``` which is final demo of sample.

# Running

Run any ```http-server``` in project root and navigate to it.

# References

More usefull information can be found [here](https://kripken.github.io/emscripten-site/docs/porting/connecting_cpp_and_javascript/Interacting-with-code.html)