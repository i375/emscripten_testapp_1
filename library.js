

function myTestJSFunction()
{
    console.log("myTestJSFunction called")
    console.log("Now calling function defined in C++")
    callExportedCFunction()
}

function readFromBuffer(ptr) {
    console.log("reading from buffer at: " + ptr+1 + " value: " + Module.getValue(ptr+1, 'i8'))
}

function callExportedCFunction()
{
    //           Calling function <addTwoVars> defined in C++ code
    var result = Module.ccall('addTwoVars', // name of C function
        'number', // return type
        ['number', 'number'], // argument types
        [10, 20]) // arguments

    console.log("added two vars: " + result)
}